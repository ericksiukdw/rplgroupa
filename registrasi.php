<?php
require_once('/pageheader.php');

?>

<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">Home</a>
        </li>
        <li class="breadcrumb-item active">Halaman Registrasi</li>
      </ol>
      <div class="row">
        <div class="col-6">
          <?php 
          if(isset($_GET["pesan"]) && $_GET["pesan"]!=""){
            echo "<span class='alert alert-success'>".$_GET["pesan"]."</span>";
          }
          ?>
          <h1>Registrasi</h1>
        <form action="prosesregis.php" method="POST">
          <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" class="form-control" name="username">
          </div>
        <div class="form-group">
          <label for="password">Password:</label>
          <input type="password" class="form-control" name="password">
        </div>
        <div class="form-group">
          <label for="repassword">Repassword:</label>
          <input type="password" class="form-control" name="repassword">
        </div>
        <div class="form-group">
          <label for="nama">Nama:</label>
          <input type="text" class="form-control" name="nama">
        </div>
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" class="form-control" name="email">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
</div>

<?php
require_once('/pagefooter.php');
?>