<?php 

/*if(!isset($_SESSION["username"])){
    header("Location: /RPLGroupA/loginform.php");
}*/

require_once('/pageheader.php');
?>

<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Tambah Produk</li>
</ol>
<div class="row">
    <div class="col-6">
        <h3>Form Tambah Produk</h3><br>
        <form action="prosestambah.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="namaproduk">Nama Produk :</label>
            <input type="text" class="form-control" name="namaproduk">
        </div>
        <div class="form-group">
            <label for="quantity">Quantity :</label>
            <input type="text" class="form-control" name="quantity">
        </div>
        <div class="form-group">
            <label for="hargabeli">Harga Beli :</label>
            <input type="text" class="form-control" name="hargabeli">
        </div>
        <div class="form-group">
            <label for="hargajual">Harga Jual :</label>
            <input type="text" class="form-control" name="hargajual">
        </div>
        <div class="form-group">
            <label for="fileToUpload">Gambar :</label>
            <input type="file" name="fileToUpload">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    </div>
</div>

<?php
require_once('/pagefooter.php');
?>