<?php

require 'vendor/autoload.php';
use Mailgun\Mailgun;


# First, instantiate the SDK with your API credentials
$mg = Mailgun::create('key-43157290191074130f3e2873103ed6b4');

# Now, compose and send your message.
# $mg->messages()->send($domain, $params);
$mg->messages()->send('ukdw.ac.id', [
  'from'    => 'erick@staff.ukdw.ac.id',
  'to'      => 'erick@si.ukdw.ac.id',
  'subject' => 'The PHP SDK is awesome!',
  'text'    => 'It is so simple to send a message.'
]);

?>